/**
* Created with BookFiendAPI.
* User: bluechiptony
* Date: 2015-06-06
* Time: 09:49 PM
* To change this template use Tools | Templates.
*/

var frisby = require('frisby');


frisby.create('Basic entry point test')
    .get('http://detect-respect.codio.io:8080/')
    .expectStatus(200)
    .expectHeader('Content-Type', 'application/json')
    .expectJSONTypes({
    message: String
}).expectJSON({
    message:'You Know why im Here'
})
    .toss();

frisby.create('Book Route test')
    .get('http://detect-respect.codio.io:8080/books')
    .expectStatus(200).expectHeader('Content-Type', 'application/json').toss();

frisby.create('nano route test')
    .get('http://detect-respect.codio.io:8080/bookie')
    .expectStatus(200).expectHeader('Content-Type', 'application/json').toss();

//booke route test
frisby.create('Booke route').get('http://detect-respect.codio.io:8080/bookie').expectStatus(200)
    .expectHeader('Content-Type', 'application/json').toss();