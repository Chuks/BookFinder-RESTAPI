/**
* Created with BookFiendAPI.
* User: bluechiptony
* Date: 2015-06-06
* Time: 09:48 PM
* To change this template use Tools | Templates.
*/

//import necessary modules
var http = require('http');
var restify = require('restify');
var request = require('request');
var rand = require('csprng');
var hash = require('sha1');

var userMod = require('./controllers/userModule.js');
var nano = require('nano')('https://tonyegwu.iriscouch.com/')

var booke = nano.db.use('read_users')

var user_db = nano.db.use('read_users')


//create restify server
var server = restify.createServer();


server.use(restify.bodyParser());
server.use(restify.authorizationParser());
server.use(restify.queryParser());


server.listen(8090, function(){
    console.log('incoming request being handled');
    
     var usere = /^\/users/
    var user_get = /^\/users\.(json|xml)$/
    var addfavs = /^\/adduserfavs/
    
    //first basic route
    server.get('/', function(req, res, next){
        res.setHeader('Content-Type', 'application/json');
        res.write(JSON.stringify({message: 'You Know why im Here'}));
        res.end();
    });
    
    //get all books route
    server.get('/books', userMod.getBook);
    
    //get single user route
    server.get(user_get, userMod.getUser);
    //end of get single user route
    
    //put request to register users into database
    //user put route
    server.put(usere, userMod.addU);
    //end of user put method
    
    
    //add favs route
    server.put(addfavs, userMod.addToFavs);
    //end of add favs route
    
    
    
    
    
    
});


    
    //    user object contains 
    //    var user = {
    //        name: name,
    //        email: email,
    //        password: password,
    //        favourites: []
    //    }
    

console.log('listening on port 8090');