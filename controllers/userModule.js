/**
* Created with BookFiendAPI.
* User: bluechiptony
* Date: 2015-06-29
* Time: 03:16 AM
* To change this template use Tools | Templates.
*/

var request = require('request');


exports.addU = function(req, res, next){
    res.setHeader('Access-Control-Allow-Origin', '*');
    var user_email = req.query.email;
        var user_fname = req.query.first_name;
        var user_lname = req.query.last_name;
        var user_password = req.query.password;
        var user_database_url = 'https://tonyegwu.iriscouch.com/read_users/'+user_email;
    
    
    
    console.log(user_database_url)
    
     var new_user = {
            first_name: user_fname,
            last_name: user_lname,
            email: user_email,
            passcode: user_password,
            favourites:[]
        };
        
        request.get(user_database_url, function(err, response, body){
           if(err){
               console.log(err)
               res.send(err);
               res.end()
           } else{
               
               //check if document exists
               if(response.statusCode == 404){
                   //if document not found
                   var stringy_doc = JSON.stringify(new_user);
                   var params  = {uri:user_database_url, body:stringy_doc};
                   //put params in database 
                   request.put(params, function(err, response, body){
                       res.setHeader('Access-Control-Allow-Origin', '*');
                        if(err){// handle error
                            return next(new restify.InternalServerError('Cant Create Document'));
                            console.log(new_user);
                            res.send(new_user);
                            res.end();
                        }

                        res.end();
                    });
               
               }else if(response.statusCode == 200){
                   //if document found
                   var error_object = {
                       name: "Book Fiend Message",
                       type: "error",
                       message: "User Email Already exixts",
                       fix: "Register With Different Email Address"
                   };
                   res.send(error_object);
                   res.end();
               }
           }
        });
}

exports.addUser = function(req, res, next){
    req.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Origin', '*');
        
        var user_email = req.query.email;
        var user_fname = req.query.first_name;
        var user_lname = req.query.last_name;
        var user_password = req.query.password;
        var user_database_url = 'https://tonyegwu.iriscouch.com/read_users/'+user_email;
        
        
        //generate new user from URL parameters
        var new_user = {
            first_name: user_fname,
            last_name: user_lname,
            email: user_email,
            passcode: user_password,
            favourites:[]
        };
        
        request.get(user_database_url, function(err, response, body){
           if(err){
               console.log(err)
               res.send(err);
               res.end()
           } else{
               
               //check if document exists
               if(response.statusCode == 404){
                   //if document not found
                   var stringy_doc = JSON.stringify(new_user);
                   var params  = {uri:user_database_url, body:stringy_doc};
                   //put params in database 
                   request.put(params, function(err, response, body){
                       res.setHeader('Access-Control-Allow-Origin', '*');
                        if(err){// handle error
                            return next(new restify.InternalServerError('Cant Create Document'));
                            console.log(new_user);
                            res.send(new_user);
                            res.end();
                        }

                        res.end();
                    });
               
               }else if(response.statusCode == 200){
                   //if document found
                   var error_object = {
                       name: "Book Fiend Message",
                       type: "error",
                       message: "User Email Already exixts",
                       fix: "Register With Different Email Address"
                   };
                   res.send(error_object);
                   res.end();
               }
           }
        });
        
}



exports.getUser = function(req, res, next){
        res.setHeader('Access-Control-Allow-Origin', '*');
        var user_email = req.query.email;
        var urlrep = user_email.replace('@', '%40')
        console.log(user_email)
        var user_database_url = 'https://tonyegwu.iriscouch.com/read_users/'+user_email;
        request.get(user_database_url, function(err, response, body){
            res.setHeader('Access-Control-Allow-Origin', '*');
           if(err){
               res.setHeader('Access-Control-Allow-Origin', '*');
               console.log(err)
               res.send(err);
               res.end();
           } else{
              
               if(response.statusCode == 200){
                   res.setHeader('Access-Control-Allow-Origin', '*');
                   console.log(body);
                   res.send(JSON.parse(body));
                   res.end();
               }else if(response.statusCode == 404){
                   console.log("Document not found")
               }
           }
        });
        
        
    }



exports.getBook = function(req, res, next){
        var isbn = req.query.isbn;
        res.setHeader('Access-Control-Allow-Origin', '*');
        
        request.get('https://www.googleapis.com/books/v1/volumes?q=isbn:'+isbn, function(err, respone, body){
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.parse(body));
            res.end();
        });
        
    }



//add book to favourite
exports.addToFavs = function(req, res, next){
        res.setHeader('Access-Control-Allow-Origin', '*');
        var user_email = req.query.id;
        var book_isbn = req.query.isbn;
        var user_database_url = 'https://tonyegwu.iriscouch.com/read_users/'+user_email;
        
        var pass_salt = rand(100, 36);
        var hash_pass = sha1(req.authorization.basic.username+pass_salt);

        var last_mod_date = new Date().toUTCString();
        
        
        console.log(user_email, book_isbn);
        //check for user email
        
        //get method to check fou user account
        //and augment favourite array
        request.get(user_database_url, function(err, response, body){
            if(err){
                
            }else{
                if(response.statusCode == 200){
                    console.log(JSON.parse(body))
                    
                    
                }else if(response.statusCode == 404){
                    console.log('cannot find document')
                }
            }
            
        });
        //end of check for user account
    }